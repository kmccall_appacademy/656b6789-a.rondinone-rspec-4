class Timer

  attr_accessor :seconds

  def initialize(seconds = 0)
    @seconds = seconds
  end

  def time_string
    seconds_given = @seconds

    sec_i = seconds_given % 60
    min_i = (seconds_given / 60) % 60
    hour_i = (seconds_given / 60) / 60

    "#{correct_digi_to_s(hour_i)}:#{correct_digi_to_s(min_i)}:#{correct_digi_to_s(sec_i)}"
  end

  def correct_digi_to_s(time)
    if time < 10
      time = "0" + time.to_s
    else
      time = time.to_s
    end
    time
  end

end
