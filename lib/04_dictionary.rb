class Dictionary
  # TODO: your code goes here!

  attr_accessor :entries, :keywords

  def initialize
    @entries = {}
  end

  def add(new_entry)
   if new_entry.is_a?(String)
     @entries[new_entry] = nil
   elsif new_entry.is_a?(Hash)
     @entries[new_entry.flatten[0]] = new_entry.flatten[1]
   end
 end

 def keywords
   result = []
   @entries.each_key {|key| result << key.to_s}
   result.sort
 end

 def include?(keyword)
   keywords.include?(keyword)
 end

 def find(str)
   result = {}
   keywords.each do |keyword|
     if keyword.include?(str)
       result[keyword] = @entries[keyword]
     end
   end
   result
 end

 def printable
   result = []
   keywords.each { |keyword| result << %Q([#{keyword}] "#{@entries[keyword]}")}
   result.join("\n")

 end



end
