class Temperature
  # TODO: your code goes here!

  attr_accessor :fahrenheit, :celsius

  def initialize(option = {})
    if option[:c]
      @celsius = option[:c]
    else
      @fahrenheit = option[:f]
    end
  end

  def self.from_fahrenheit(temp)
    self.new(f: temp)
  end

  def self.from_celsius(temp)
    self.new(c: temp)
  end


  def in_fahrenheit
    @fahrenheit ? @fahrenheit : self.class.ctof(@celsius)

  end

  def in_celsius
    @celsius ? @celsius : self.class.ftoc(@fahrenheit)
  end


  def self.ftoc(temperature)
    (temperature - 32) * 5 / 9.0
  end

  def self.ctof(temperature)
    temperature * 9 / 5.0 + 32
  end
end

class Celsius < Temperature
  def initialize(temp)
    @celsius = temp
  end
end

class Fahrenheit < Temperature
  def initialize(temp)
    @fahrenheit = temp
  end
end
